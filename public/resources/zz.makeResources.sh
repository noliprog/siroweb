#!/bin/bash

ImgFileNames=();                   ImgSizeXs=(); ImgSizeYs=();     ImgPosXs=();      ImgPosYs=();      ImgRots=(); ImgNewNames=(); Qualities=()

ImgFileNames+=("../../../../Photos/PhotosGeek08/20240209_173132.jpg"); CropSizeXs+=(4128); CropSizeYs+=(3096); CropPosXs+=(0); CropPosYs+=(0);  NewImgSizeXs+=(1032); NewImgSizeYs+=(774); ImgRots+=(0); Qualities+=(0); ImgNewNames+=("2024-02-09.GravignyRainbow");
ImgFileNames+=("../../../../Photos/PhotosGeek08/20210414_160159.jpg"); CropSizeXs+=(2912); CropSizeYs+=(810); CropPosXs+=(776); CropPosYs+=(1020);  NewImgSizeXs+=(1456); NewImgSizeYs+=(405); ImgRots+=(0); Qualities+=(75); ImgNewNames+=("2021-04-14.ParcTempliers");

for i in "${!ImgFileNames[@]}"; do
    ImgFileName="${ImgFileNames[i]}"
    CropSizeX="${CropSizeXs[i]}"
    CropSizeY="${CropSizeYs[i]}"
    CropPosX="${CropPosXs[i]}"
    CropPosY="${CropPosYs[i]}"
    NewImgSizeX="${NewImgSizeXs[i]}"
    NewImgSizeY="${NewImgSizeYs[i]}"
    ImgRot="${ImgRots[i]}"
    ImgNewName="${ImgNewNames[i]}"
    Quality="${Qualities[i]}"

    if [ "$Quality" == "0" ]; then
	extension=".png"
    else
	extension=".jpg"
    fi

    convert $ImgFileName -crop "$CropSizeX"x"$CropSizeY"+"$CropPosX"+"$CropPosY" -resize "$NewImgSizeX"x"$NewImgSizeY"! -rotate $ImgRot -quality $Quality $ImgNewName$extension
done

convert "${ImgNewNames[0]}".png -region 8x32+799+128 -blur 0x10 +region -region 8x23+777+58 -blur 0x10 +region -rotate 90 -quality 75 "${ImgNewNames[0]}".jpg
rm -f "${ImgNewNames[0]}".png
