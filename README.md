# Objectifs
- Faire un petit journal.
- Apprendre à s'exprimer par écrit en public.
- Découverte des outils pour réaliser des sites web statiques.

# Règles
<ul>
  <li>Il ne faut pas injurier</li>
  <li>Il ne faut pas publier d'informations personnelles
  <ul>
    <li>Il ne faut pas donner les noms de famille, les numéros de téléphone, les adresses...</li>
    <li>Il faut flouter les visages, les plaques d'immatriculation...</li>
  </ul>
  </li>
</ul>

# Les pseudonymes
- Noliprog (papa).
- Mario (le fils aîné)
- Sonic (le fils cadet)

# Links
[Le site web](https://noliprog.gitlab.io/siroweb)
